### Preprocessing the UNSW-NB15

The dataset used in this study was the PCAP files obtained from the [UNSW-NB15](https://www.unsw.adfa.edu.au/unsw-canberra-cyber/cybersecurity/ADFA-NB15-Datasets/) site.

The following programs/scripts should be executed in this order:

1. Separate each individual flow into its own PCAP file. 

        $ ./scriptflow.sh

2. The following step is to select all malicious flows and add a resume of them into the <attack.csv>.

        $ python select_flows.py

3. Then, the individual flows are going to be classified. If it is a malicious flow, <MAL> is going to be added to its name. If it is a malfuncioning file, <BAD> is going to be added to its name.

        $ python identify.py

4. Finally, all the flows are going to be adapted to the chosen topology by using:

        $ python convert_to_topo.py
	


