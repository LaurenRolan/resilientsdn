from scapy.all import *
import pandas as pd
import csv
import glob
import os

# The objective of this code is to identify the flows which are malicious.
# Based on the csv containing a resume of each flow and its classification
# it is possible to rename the files containing malicious traffic.
if __name__ == '__main__':
    data = []
    header = []
    #Gets the 4 files containing all the flows
    for arq in range(1, 5):
        with open("UNSW-NB15_"+ str(arq) + '.csv', 'rb') as readFile:
            data.append(pd.read_csv(readFile, sep=',', header=None))
    datum = pd.concat(data)

    #Gets the header (name of each column/attribute)
    with open("NUSW-NB15_features.csv", 'rb') as readFile:
        reader = csv.reader(readFile)
        next(reader)
        for line in reader:
            header.append(line[1])
    
    #Gets the list of attacks
    with open("attacks.csv", 'rb') as readFile:
        attacks = pd.read_csv(readFile, sep=';', header=None)
    datum.columns = header
    attacks.columns = header

    for pcap in range(1, 28):
        for filename in glob.glob(str(pcap)+'-*.pcap'):
            print filename
            if str(pcap)+"-stream-" in filename:
                packets = rdpcap(filename)
		#If there is no packets in the file, it is named a BAD file
                if len(packets) == 0:
                    os.rename(filename, "BAD-" + filename)
                    print "Incomplete data at " + filename
		#Else, compare the flow in the PCAP with all the malicious
		#flows listed at attack.csv
                else:
                    first = packets[0]
                    srcIP = first['IP'].src
                    dstIP = first['IP'].dst
                    srcPort = first['TCP'].sport
                    dstPort = first['TCP'].dport
                    same_hosts = attacks.loc[(attacks['srcip'] == srcIP) & (attacks['dstip'] == dstIP)]
                    same_ports = same_hosts.loc[(attacks['sport'] == srcPort) & (attacks['dsport'] == dstPort)]
		    #If found a malicious flow, label it
                    if(not same_ports.empty):
                        os.rename(filename, "MAL-"+filename)
                        print "Found it at " + filename
