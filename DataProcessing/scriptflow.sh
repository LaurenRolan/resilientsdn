#!/bin/bash
for number in {1..28}
do
   for stream in `tshark -r $number.pcap -T fields -e tcp.stream | sort -n | uniq`
   do
      echo $stream
      tshark -r $number.pcap -w $number-stream-$stream.pcap -2 -R "tcp.stream==$stream"
   done
done
