import csv
# This code intends to find all flows labeled as attack and add the resume
#of each os these flows to a csv file

if __name__ == '__main__':
    arq = 1
    with open("attacks.csv", "wb") as w:
        writer = csv.writer(w, delimiter=';', quotechar='"', quoting=csv.QUOTE_ALL)
        while arq <= 4:
            with open('UNSW-NB15_'+str(arq)+'.csv', 'rb') as f:
                print "Reading UNSW-NB15_" + str(arq)
                reader = csv.reader(f)
                for line in reader:
		    #Checks the attack flag of each flow
                    attack = line[48]
                    if attack == "1":
                        writer.writerow(line)
            arq += 1
            f.close()
    w.close()
