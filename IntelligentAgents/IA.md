### Important steps on Intelligent Agents

The following steps must be run __after__ the preprocessing phase.

1. Gathers the necessary information from each flow and adds it to a table (wich constitutes a folder). It does so ten times.

        $ python get_folds.py

This module will make use of the _packet.py_ file, which uses Scapy in order to extract features from the PCAP files.

2. The following step is applied to test the accuracy and F-measure of logistic regression and Naïve Bayes. Both show a __low performance__ in F-measure, which means that they predict a large rate of false negatives. 

        $ python reward_agent.py

3. The next step, __not implemented yet__, is the development of the deep reinforcement network. It is suggested to use some pre-built Python modules, such as [PyTorch](https://pytorch.org/).

        $ python dqn.py

4. Finally, the reward agent, the dqn agent and the flows must be adapted to a Mininet topology.
