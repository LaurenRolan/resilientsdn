import pandas as pd
import csv
import random as rd
from scapy.all import *

def getNums():
    with open('../../UNSW-NB15/attack_count.csv', 'r') as readFile:
        attacksFile = csv.reader(readFile, delimiter='\t')
        next(attacksFile)
        countAttack = 0
        for line in attacksFile:
            countAttack += 1

    with open('../../UNSW-NB15/normal_count.csv', 'r') as readFile:
        normalsFile = csv.reader(readFile, delimiter='\t')
        next(normalsFile)
        countNormal = 0
        for line in normalsFile:
            countNormal += 1
    return countAttack, countNormal

def kfold(k):
    folds = []

    with open('../../UNSW-NB15/attack_count.csv', 'r') as readFile:
        attacksFile = pd.read_csv(readFile, delimiter='\t')
    with open('../../UNSW-NB15/normal_count.csv', 'r') as readFile:
        normalsFile = pd.read_csv(readFile, delimiter='\t')

    numAttacks, numNormals = getNums()
    numTotal = numNormals + numAttacks

    percAttacks = float(numAttacks) / numTotal

    numPerFold = int(numTotal / k)
    numExtra = numTotal % k

    for f in range(0, k):
        print "Fold " + str(f)
        if numExtra > 0:
            numExtra -= 1
            numInFold = numPerFold + 1
        else:
            numInFold = numPerFold

        numAttackPerFold = int(numInFold * percAttacks)
        numNormalPerFold = numPerFold - numAttackPerFold

        selectedAttacks = rd.sample(range(numAttacks), numAttackPerFold)
        selectedNormals = rd.sample(range(numNormals), numNormalPerFold)

        attacksData= attacksFile.ix[selectedAttacks]
        attacksFile.drop(selectedAttacks)

        normalData= normalsFile.ix[selectedNormals] #, ignore_index=True
        normalsFile.drop(selectedNormals)

        foldData = pd.concat([attacksData, normalData], ignore_index=True)

        folds.append(foldData)

    return folds

