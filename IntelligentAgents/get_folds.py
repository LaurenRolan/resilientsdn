

import crossval as crv
from scapy.all import *
import packet
import pandas as pd

size = 10

if __name__ == "__main__":
    folds = crv.kfold(10)
    test = []
    tables = []
    dois = [folds[0], folds[1]]

    #Gets data from all PCAPs, preprocesses it and create dataframes
    for fold in folds:
        for index, row in fold.iterrows():
            filename = row.ix[0]
            if os.path.isfile("../../UNSW-NB15/" + filename):
                print "\n\nFile " + filename
                packets = rdpcap("../../UNSW-NB15/" + filename)
                if 'MAL' in filename:
                    MAL = 1
                else:
                    MAL = 0
                if len(packets) != 0:
                    if (len(packets) < size):
                        size = len(packets)
                    for i in range(0, len(packets) - size + 1, size):
                        tableFile = packet.buildTable(packets, i, size, MAL)
                        test.append(tableFile)
        table = pd.concat(test)
        tables.append(table)

    for f in range(0, len(folds)): #len(tables)
        print "Writing to fold-" + str(f) + '.csv'
        tables[f].to_csv('fold-'+str(f)+'.csv')

