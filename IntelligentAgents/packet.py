import statistics
import numpy as np
import pandas as pd

def buildTable(packets, i, size, MAL):
    data = []
    cols = ['BPS Mean', 'BPS Variance', 'BPS Max', 'BPS Min', 'PPS Mean', 'PPS Max', 'PPS Min', 'PL Variance', 'PL Max',
            'PL Min', 'PL 1st Quartile', 'Flow Bytes', 'PIAT Mean', 'PIAT Min', 'PIAT 3rd Quartile', 'Flow Duration',
            'Flow Packets', 'target']
    data = getBytesPerSec(packets, i, size)
    data.extend(getPacketsPerSec(packets, i, size))
    data.extend(getPacketsLength(packets, i, size))
    data.extend(getTimeArrival(packets, i, size))
    data.append(size)
    data.append(MAL)
    dataframe = pd.DataFrame([data], columns=cols)
    return dataframe


def getTimeArrival(packets, i, size):
    inter_arrival = []
    for num in range(i + 1, i + size):
        #Inter-arrival time in MILISSECONDS
        inter_arrival.append((packets[num].time - packets[num-1].time) * 1000)
    mean = statistics.mean(inter_arrival)
    mini = min(inter_arrival)
    third_quart = np.percentile(inter_arrival, 75)
    duration = packets[i + size - 1].time - packets[i].time
    return [mean, mini, third_quart, duration]

def getPacketsLength(packets, i, size):
    lengths = []
    tot = len(packets[i])
    for num in range(i, i + size):
        lengths.append(len(packets[num]))
        tot += len(packets[num])
    maxi = max(lengths)
    mini = min(lengths)
    if len(lengths) == 1:
        vari = 0.0
    else:
        vari = statistics.variance(lengths)
    first_quart = np.percentile(lengths, 25)
    return [vari, maxi, mini, first_quart, tot]

def getBytesPerSec(packets, i, size):
    bps = []
    for num in range(i + 1, i + size):
        if packets[num].time - packets[num - 1].time != 0:
            bps.append(len(packets[num]) / ((packets[num].time - packets[num - 1].time) * 1000))
        else:
            bps.append(0)
    if len(bps) == 1:
        mean = bps[0]
        vari = 0.0
    else:
        mean = statistics.mean(bps)
        vari = statistics.variance(bps)
    maxi = max(bps)
    mini = min(bps)
    return [mean, vari, maxi, mini]

def getPacketsPerSec(packets, i, size):
    pps = []
    for num in range(i + 1, i + size):
        if packets[num].time - packets[num - 1].time != 0:
            pps.append(size / ((packets[num].time - packets[num - 1].time) * 1000))
        else:
            pps.append(0)
    if len(pps) == 1:
        mean = pps[0]
    else:
        mean = statistics.mean(pps)
    maxi = max(pps)
    mini = min(pps)
    return [mean, maxi, mini]
