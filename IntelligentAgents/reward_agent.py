from sklearn.metrics import accuracy_score, f1_score
from sklearn.naive_bayes import GaussianNB
from sklearn.linear_model import LogisticRegression
from sklearn.decomposition import PCA
from sklearn.feature_selection import SelectKBest, chi2, f_regression, f_classif, mutual_info_regression
import pandas as pd
import copy

tables = []

#Reads .csv files into dataframes
for f in range(0, 10):
    print "Reading fold-" + str(f) + ".csv"
    with open('fold-'+str(f)+'.csv', 'r') as readFile:
        tables.append(pd.read_csv(readFile))

'''
Iterates over k-best components, 
starting from 3 and ending at 15.
'''
for comp in range(3, 15, 2):
    print "Using %d components..." % (comp)

    #Name the current header
    comps = ['comp-'+str(i) for i in range(0, comp)]
    print comps

    #For each folder
    for f in range(0, 10):
        print "Fold #" + str(f)

        #Gets the test fold and the training folds
        test_fold = tables[f]
        train_folds = copy.deepcopy(tables)
        train_folds = pd.concat(train_folds)
        train_folds.drop(test_fold)

        X = train_folds[train_folds.columns.drop('target')]
        y = train_folds['target']

        #Selects the k-best features
        X_PC = SelectKBest(f_regression, k=comp).fit_transform(X, y)
        X_df = pd.DataFrame(data=X_PC, columns=comps)

        #Indicates which are the k-best features
        print list(X_df)

        test = test_fold[test_fold.columns.drop('target')]
        y_obj = test_fold['target']

        test_PC = SelectKBest(f_regression, k=comp).fit_transform(test, y_obj)
        test_df = pd.DataFrame(data=test_PC, columns=comps)
        print list(test_df)

        #Trains the Naive Bayes algorithm and obtains its measurements
        print "\n\nExecuting Naive Bayes..."
        gnb = GaussianNB()
        y_pred = gnb.fit(X_df.values, y).predict(test_df.values)

        acc = accuracy_score(y_obj, y_pred)
        fm = f1_score(y_obj, y_pred)

        print "---------------------------------"
        print "Accuracy of testing fold #" + str(f)
        print acc
        print "---------------------------------"
        print "F1-score of testing fold #" + str(f)
        print fm

        #Trains the Logistic Regression algorithm and obtains its measurements
        print "\nExecuting Logistic Regression..."
        logReg = LogisticRegression(solver='saga')
        y_pred = logReg.fit(X_df.values, y).predict(test_df.values) #test_df
        #y_fin = pd.DataFrame(y_pred, columns=['target'])

        acc = accuracy_score(y_obj, y_pred) #y_obj
        fm = f1_score(y_obj, y_pred) #y_obj

        print "Accuracy of testing fold #" + str(f)
        print acc
        print "---------------------------------"
        print "F1-score of testing fold #" + str(f)
        print fm
        print "---------------------------------"
