# ResilientSDN

A project developed by Lauren Silva Rolan Sampaio, a Computer Engineering undergrad student at Universidade Federal do Rio Grande do Sul (UFRGS).

---

### Directories
* __IntelligentAgents__  
Contains both the deep reinforcement learning agent and the reward agent, which is implemented using a Na�ve Bayes algorithm. Both agents are implemented with Python.

* __DataProcessing__  
Contains the scripts and Python programs used to preprocess the [UNSW-NB15](https://www.unsw.adfa.edu.au/unsw-canberra-cyber/cybersecurity/ADFA-NB15-Datasets/) dataset.

* __PerformanceEvaluator__  
Contains the Python scripts to the generation of graphics.

---

### Using Mininet
